# Testiatest

Technical test project for Testia

## Reminder

Install [NPM / NODE.JS](https://nodejs.org)\
Run `npm install` for dependencies

## Launch dev server

Run `npm run start`

## Launch electron app

Run `npm run electron:start`

## Build Windows app executable

Run `npm run electron:win`

## Contributors

Matthieu Montaillé
