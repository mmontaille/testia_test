import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class NetworkService {

  // Network observable
  online: BehaviorSubject<Boolean> = new BehaviorSubject<Boolean>(navigator.onLine);

  constructor() {
    // Listening to online event
    window.addEventListener('online', () => {
      this.online.next(true);
    });
    // Listening to offline event
    window.addEventListener('offline', () => {
      this.online.next(false);
    });
  }

  getNetwork() {
    return navigator.onLine;
  }

}
