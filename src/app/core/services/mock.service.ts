import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class MockService {

  constructor(private http: HttpClient) { }

  // Sending form to jsonstub
  sendForm(data: any) {
    return this.http.post(`${environment.API}/defaut`,
      data,
      {
        headers: new HttpHeaders({
          'Content-Type': 'application/json',
          'JsonStub-User-Key': '32bd6b40-d748-49a3-b46c-11a1c9953813',
          'JsonStub-Project-Key': '84bd807f-44cf-41eb-87ad-89f7c84dbf93'
        })
      });
  }

}
