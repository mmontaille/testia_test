import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators, FormGroupDirective } from '@angular/forms';
import { MockService } from '../core/services/mock.service';
import { MatSnackBar } from '@angular/material';
import { NetworkService } from '../core/services/network.service';

@Component({
  selector: 'app-form',
  templateUrl: './form.component.html',
  styleUrls: ['./form.component.scss']
})
export class FormComponent implements OnInit {

  // Form init
  form = new FormGroup({
    defect: new FormControl(null, [Validators.required]),
    name: new FormControl(null, [Validators.required, Validators.pattern(/^([^0-9]*)$/)]),
    comment: new FormControl(null),
    date: new FormControl(new Date())
  });

  // Defects list
  defects = ['Délaminage', 'Porosité', 'Inclusion'];

  // Online observable
  online$ = this._network.online;

  // Request loading boolean
  loading = false;

  constructor(private _mock: MockService,
    private _snackbar: MatSnackBar,
    private _network: NetworkService) {
  }

  ngOnInit() {
  }

  // Form submit
  submit(formDirective: FormGroupDirective) {
    // Check if online
    if (this._network.getNetwork()) {
      // Check if form is valid and no ongoing request
      if (this.form.valid && !this.loading) {
        this.loading = true;
        this._mock.sendForm(this.form.value).subscribe(
          success => {
            this.loading = false;
            this._snackbar.open('Formulaire envoyé avec succès', '', { duration: 3000 });
            formDirective.resetForm(); // Validators reset
            this.form.reset(); // Fields reset
            this.form.get('date').setValue(new Date()); // Date init
          },
          error => {
            this.loading = false;
            this._snackbar.open('Erreur d\'envoi du formulaire', '', { duration: 3000 });
          }
        );
      }
    } else {
      this._snackbar.open('Vous êtes hors-ligne, impossible d\'envoyer le formulaire');
    }
  }

}
